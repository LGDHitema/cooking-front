<?php include "inc/header.php" ?>

<a href="index.php">> Retour à l'accueil</a>
<br><br>
<h2>Mon profil</h2><br>
<?php
   if (isset($_GET['return'])) {
      if ($_GET['return'] == 1) {
         echo "Les informations ont été changées.";
      }
   }
?>
<p>
   N'hésitez pas à modifier vos informations personnelles si celles-ci ont changé.
</p>
<br>
<div class="row" id="cadreProfil">
   <div class="col-sm-8" id="profilInfos">
      <br>
      <fieldset >
         <form method="POST" action="post/modif-profil.php">
      <?php
         $data = $bdd->query('SELECT * FROM membres WHERE idMembre = "' . $_SESSION['idMembre'] . '"');
         $datas = $data->fetch(); ?>
            <div class="row">
               <div class="col-sm text-right">
                  <p>Pseudo :</p>
               </div>
               <div class="col-sm float-left">
                  <p><input type="text" name="login" value ="<?= $datas['login'] ?>"></p>
               </div>
            </div>
            <div class="row">
               <div class="col-sm text-right">
                  <p>Nom :</p>
               </div>
               <div class="col-sm float-left">
               <p><input type="text" name="nom" value ="<?= $datas['nom'] ?>"></p>
               </div>
            </div>
            <div class="row">
               <div class="col-sm text-right">
                  <p>Prenom :</p>
               </div>
               <div class="col-sm float-left">
               <p><input type="text" name="prenom" value ="<?= $datas['prenom'] ?>"></p>
               </div>
            </div>
            <div class="row">
               <div class="col-sm text-right">
                  <p>Adresse e-mail :</p>
               </div>
               <div class="col-sm float-left">
               <p><input type="mail" name="mail" value ="<?= $datas['mail'] ?>"></p>
               </div>
            </div>
            <input type="submit" value="Modifier les infos">
         </form>
      </fieldset><br>
      <input type="checkbox" name="newsletter" value="newsletter"> S'inscrire à la newsletter
      <br><br>

      <br><br>

      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Suppression du compte</button>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Suppression du compte</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <p>Êtes-vous bien sûr.e de vouloir supprimer votre compte ? Cela supprimera aussi (.....ou pas, à voir) vos recettes partagées.</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" onclick="javascript:location.href='post/delete-profil.php'">Supprimer</button>
               </div>
            </div>
         </div>
      </div>

      <hr>
      <br>
      <h3>Mes recettes proposées :</h3>
      <?php
         $l = $_SESSION['idMembre'];
         //$r = $bdd->query("SELECT * FROM recettes r, membres m WHERE r.membre = m.idMembre AND m.login = ' . $l . '");
         $r = $bdd->query("SELECT * FROM vue_recettes_personnes WHERE login = ' . $l . '");
         $recettes = $r->fetchAll();
         if (!$recettes) { ?>
            <p> -> Vous ne nous avez pas encore proposé de recettes ! <a href="ajouter-recette.php">Commencez dès aujourd'hui °w° !</a></p>
         <?php } else {
            foreach ($recettes as $rs) ?>
               <p>
                  <ul>
                     <li>
                     <a href="recette-detail.php?id= <?= $rs['idRecette'] ?>"><?= $rs['titre'] ?></a>
                     <button type="button" class="btn btn-info" data-toggle="modal" data-target=""><i class="fas fa-edit"></i></button>
                     <button type="button" class="btn btn-danger" data-toggle="modal" data-target=""><i class="fas fa-trash-alt"></i></button>
                     </li> 
                  </ul>
               </p>
         <?php } ?> 
         
   </div>
   <div class="col-md-1 col-md-offset-1"></div>
   <div class="col-sm" id="profilDetails">
      <h3>Photo de profil</h3>
      <br>
         <?php
            $log = $_SESSION['idMembre'];
            $gravatar = $bdd->query('SELECT * FROM membres WHERE idMembre = "' . $log . '"');
            $avatar = $gravatar->fetch();
            if (!isset($avatar['gravatar'])) { ?>
               <div>
                  <span id='gravatar'></span>
               </div>
               <br>
               <a href='#'>Ajouter un avatar</a>
            <?php } else { ?>
               <div>
                  <img src="photos/gravatars/<?=$avatar['gravatar'];?>" id="avatar">
               </div>
               <br><br>
               <div style="width:25%;">
                  <!--<form method="POST" action="upload.php" enctype="multipart/form-data">
                     <input type="hidden" name="MAX_FILE_SIZE" value="100000">
                     Fichier : <input type="file" name="avatar"><br>
                     <input type="submit" name="envoyer" value="Envoyer">
                  </form> 
                  https://antoine-herault.developpez.com/tutoriels/php/upload/ 
                  
                  <form> ~> Bootstrap one
                     <div class="form-group">
                        <label for="exampleFormControlFile1">Example file input</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1">
                     </div>
                  </form> -->
               </div>
            <?php } ?>
      <hr>
      <p>
         Mes sites :
      </p>
      <div class="row">
         <div class="col">
            <p><a href="#"><img src="images/facebook.png" alt="Facebook page"></a></p>
            <input type="text">
         </div><br><br>
         <div class="col">
            <p><a href="#"><img src="images/twitter.png" alt="Twitter account"></a></p>
            <input type="text">
         </div>
         <div class="col">
            <p><a href="#"><img src="images/google.png" alt="Google+ account"></a></p>
            <input type="text">
         </div>
         <div class="col">
            <p><a href="#"><img src="images/youtube.png" alt="Google+ account"></a></p>
            <input type="text"><br>
         </div><br><br>
         <div class="col">
            <input type="submit"><br><br>
         </div>
      </div>
   </div>
</div>
	
<?php include "inc/footer.php";