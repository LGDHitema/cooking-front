<?php include "inc/header.php" ?>

<br>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner" >
	<?php
		$slider = $bdd->query('SELECT * FROM vue_recettes_personnes ORDER BY dateCrea DESC LIMIT 0,3');
		$active = "active";
		$event = "Recette du jour !";
		while ($donnees = $slider->fetch()) { ?>
			<div class='carousel-item <?= $active ?>'>
				<a href='recette-detail.php?id=<?= $donnees['idRecette'] ?>'><img class='d-block w-100' src='photos/recettes/<?= $donnees['img'] ?>' alt='<?= $donnees['idRecette'] ?>'></a>
				<div class='carousel-caption d-none d-md-block'>
					<h5><?= $event ?></h5>
					<p><?= $donnees['titre'] ?></p>
				</div>
			</div>
		<?= $active = "";
		$event = "Précédente recette phare !";
		} ?>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<br>

<hr>

<br>
<h2 id="titreIndex">Recettes des internautes</h2><br>
<div class="row" id="recettes">
		<?php
			$cards = $bdd->query('SELECT * FROM vue_recettes_personnes ORDER BY dateCrea ASC LIMIT 0,3');
			while($donneesCards = $cards->fetch()) { ?>
				<div class='col-12 col-md-6 col-lg-4 mt-3'>
					<div class='card'
					<?php switch ($donneesCards['couleur']) {
								case "bleuClair":
									echo "style = 'border-top-color:lightblue; border-bottom-color:lightblue; border-width:5px;'";
								break;
								case "vertClair":
									echo "style = 'border-top-color:lightgreen; border-bottom-color:lightgreen;border-width:5px;'";
								break;
								case "fushia":
									echo "style = 'border-top-color:pink; border-bottom-color:pink;border-width:5px;'";
								break;
								default:
								break;
							} ?> >
					<a href='recette-detail.php?id=<?= $donneesCards['idRecette'] ?>'><img class='card-img-top img-fluid' src='photos/recettes/<?= $donneesCards['img'] ?>' alt='Recette proposée'></a>
					<div class='card-body'>
						<h5 class='card-title'><?= $donneesCards['titre'] ?></h5>
						<p class='card-text justify'><?= $donneesCards['chapo'] ?></p>
					</div>
					<div class='card-footer'>
						<h5>Proposée par :</h5>
						<img src='photos/gravatars/<?= $donneesCards['gravatar'] ?>' alt='membre' class='img-thumbnail'><br><br>
						<p><a href="membre-detail.php"><?= $donneesCards['prenom'] ?></a></p>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

<?php include "inc/footer.php";