<?php include "inc/header.php" ?>

<a href="index.php">> Retour à l'accueil</a>
<br><br>
<div class="container">
   <?php 
   if(!isset($_SESSION['idMembre'])) {
      echo '<p>Vous devez être connecté.e pour nous partager vos créations culinaires ! <a href="connexion.php">Connectez-vous</a> dès à présent, ou <a href="inscription.php">rejoingez-nous</a> !</p>'; 
   } else {
      /////// Pourquoi mon fieldset ne s'affiche pas T.T ???
   ?>
   <fieldset>
      <legend>Proposez-nous une recette !</legend><br>
      <form method="POST" action="post/recette-post.php" id="recette">
         <div class="row">
            <div class="col-sm text-right">
               <p>Titre de la recette :</p>
            </div>
            <div class="col-sm float-left">
               <p><input type="text" name="titre" placeholder="Entrez le nom de votre recette" required></p>
            </div>
         </div>
         <div class="row">
            <div class="col-sm text-right">
               <p>Résumé de la recette :</p>
            </div>
            <div class="col-sm float-left">
               <textarea name="chapo" col="5" rows="10" placeholder="Entrez une description alléchante pour votre recette !" required></textarea>
            </div>
         </div><br>
         <div class="row">
            <div class="col-sm text-right">
               <p>Montrez-nous une photo de votre recette !</p>
            <form method="POST" action="upload.php" enctype="multipart/form-data">
               <input type="hidden" name="MAX_FILE_SIZE" value="100000">
               Fichier : <input type="file" name="avatar">
            </div>
            <div class="col-sm float-left">
               <input type="submit" name="envoyer" value="Envoyer le fichier">
            </form>
            </div>
         </div>
         </div>
         <hr>
         <h4>Préparation :</h4>
         <div class="row">
            <div class="col-sm text-right">
               <p>Temps de préparation :</p>
            </div>
            <div class="col-sm float-left">
            <p><input type="number" name="tempsPreparation" min="5" max="240" step="5" required> minutes</p>
            </div>
         </div>
         <div class="row">
            <div class="col-sm text-right">
               <p>Temps de cuisson :</p>
            </div>
            <div class="col-sm float-left">
            <p><input type="number" name="tempsCuisson" min="0" max="180" step="5" required> minutes</p>
            </div>
         </div>
         <div class="row">
            <div class="col-sm text-right">
               <p>Pour combien de personnes :</p>
            </div>
            <div class="col-sm float-left">
               <p>
                  <select value="parts" required>
                     <option value="1">1</option>
                     <option value="2">2</option>
                     <option value="3">3</option>
                     <option value="4">4</option>
                     <option value="6">6</option>
                     <option value="8">8</option>
                     <option value="10">10</option>
                     <option value="12">12</option>
                     <option value="14">14</option>
                     <option value="16">16</option>
                  </select>
               </p>
            </div>
         </div>
         <div class="row">
            <div class="col-sm text-right">
               <p>Coût moyen :</p>
            </div>
            <div class="col-sm float-left">
               <p>
                  <select name="prix" required>
                     <option value="Pas cher">Pas cher</option>
                     <option value="Abordable">Abordable</option>
                     <option value="Coûteux">Coûteux</option>
                  </select>
               </p>
            </div>
         </div>
         <div class="row">
            <div class="col-sm text-right">
               <p>Difficulté moyennne :</p>
            </div>
            <div class="col-sm float-left">
               <p>
                  <select value="difficulte" required>
                     <option value="Facile">Facile</option>
                     <option value="Moyen">Moyen</option>
                     <option value="Difficile">Difficile</option>
                  </select>
               </p>
            </div>
         </div>
         <hr>
         <h4>Ingrédients :</h4>
         <div class="row">
            <div class="col-sm text-right">
               <p>Ingrédients requis : </p>
               <p><input type="text" name="titre" value="" id="ingredientsRequis">
               <br>
               <button type="button" onclick="addIngredient()">Ajouter ces ingrédients</button><br>
            </div>
            <div class="col-sm float-left">
               <!--<label for="ingredient">Liste des ingrédients : <input type="text" id="liste" disabled>  textarea $_POST(''liste)-->
               <!--<input type="textarea" name="ingredients" id="liste" disabled>-->
               <!--<input type="textarea" name="ingredients" id="liste" disabled style="height:300px; word-wrap: break-word; word-break: break-all;">-->
               <textarea name="description" form="recette" id="liste" disabled></textarea> 
            </div><br>
            <div>

            </div>
         </div>
         <hr>
         <h4>Description de la préparation :</h4>
            <p><textarea name="description" rows="15" cols="120" placeholder="Entrez le déroulement de votre recette" required></textarea></p>
         <input type="submit">
      </form>
   </fieldset>

<script>
function addIngredient() {
      let div_ingredient = document.getElementById('ingredientsRequis');
      let div_liste = document.getElementById('liste');

      let my_new_secret_ingredient = div_ingredient.value;
      //console.log(div_liste.value);

      div_ingredient.value = "";

      if (div_liste.value == '') {
         div_liste.value = my_new_secret_ingredient;
      } else {
         div_liste.value +=", " + my_new_secret_ingredient;
      }
   }
</script>
<?php } ?>

</div>
<?php include "inc/footer.php";