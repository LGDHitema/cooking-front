<?php
session_start(); 

try {
   $bdd = new PDO('mysql:host=localhost;dbname=cooking;charset=latin1', 'root', '');
	} catch(Exception $e) { 
	die('Erreur : '.$e->getMessage()); 
}

   if (isset($_POST)) {
      try {
      $log = ($_SESSION['idMembre']);
      $req = "SELECT * FROM membres WHERE idMembre = '" . $log . "'";
      $whatevs = $bdd->query($req);
      $oldData = $whatevs->fetch();

      //$request = 'UPDATE membres SET (login, nom, prenom, password, mail) VALUES (:login, :nom, :prenom, :password, :mail) WHERE idMembre = "' . $log . '"';
      $request = 'UPDATE membres SET login=:login, nom=:nom, prenom=:prenom, mail=:mail WHERE idMembre = "' . $log . '"';
      $edit = $bdd->prepare($request);
      $edit->execute(array(
         'login' => filter_var($_POST['login'], FILTER_SANITIZE_STRING),
         'nom' => filter_var($_POST['nom'], FILTER_SANITIZE_STRING),
         'prenom' => filter_var($_POST['prenom'], FILTER_SANITIZE_STRING),
         'mail' => filter_var($_POST['mail'], FILTER_SANITIZE_EMAIL)
      ));

      if (($oldData['login'] !== $_POST['login']) || ($oldData['nom'] !== $_POST['nom']) || ($oldData['prenom'] !== $_POST['prenom']) || ($oldData['mail'] !== $_POST['mail'])) {
         $dataChanged = 1;
      }
   
      $path = "location: ../profil.php?return=" . (isset($dataChanged) ? 1 : 0); //opération ternaire
      header($path);
      exit();
   } catch (Exception $e) {
      die('Erreur : '.$e->getMessage()); 
   }
}

?>