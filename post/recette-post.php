<?php
session_start(); 

try {
   $bdd = new PDO('mysql:host=localhost;dbname=cooking;charset=latin1', 'root', '');
	} catch(Exception $e) { 
	die('Erreur : '.$e->getMessage()); 
}

//*********************************** INSERT *********************************** //

if (!empty($_POST)) {
   try {
      //$request = 'INSERT INTO recettes (titre, chapo, tempsPreparation, tempsCuisson, prix, difficulte, ingredient, preparation) VALUES (:titre, :chapo, :tempsPreparation, :tempsCuisson, :prix, :difficulte, :ingredient, :preparation)';

      $request = 'INSERT INTO recettes SET titre=:titre, chapo=:chapo, tempsPreparation=:tempsPreparation, tempsCuisson=:tempsCuisson, parts=:parts, prix=:prix, difficulte=:difficulte, ingredient=:ingredient, preparation=:preparation';
      
      //insert membre = $_SESSION['idMembre] !!


      // ******************************** !!! ****************************** //
      // -> Faire une page entière pour tout ce qui est sur les recettes (ajout/modif/suppr) en post/get avec Ajax
      // -> Faire un form plus joli en bootstrap que de simples inputs moches
      // ******************************** !!! ****************************** //


      $req = $bdd->prepare($request);
      $req->execute(array(
         'titre' => filter_var($_POST['titre'], FILTER_SANITIZE_STRING),
         'chapo' => filter_var($_POST['chapo'], FILTER_SANITIZE_STRING),
         'tempsPreparation' => filter_var($_POST['tempsPreparation'], FILTER_SANITIZE_NUMBER_INT),
         'tempsCuisson' => filter_var($_POST['tempsCuisson'], FILTER_SANITIZE_NUMBER_INT),
         'parts' => filter_var($_POST['parts'], FILTER_SANITIZE_NUMBER_INT),
         'prix' => filter_var($_POST['prix'], FILTER_SANITIZE_STRING),
         'difficulte' => filter_var($_POST['difficulte'], FILTER_SANITIZE_STRING),
         'ingredient' => filter_var($_POST['ingredient'], FILTER_SANITIZE_STRING),
         'preparation' => filter_var($_POST['preparation'], FILTER_SANITIZE_STRING),
      ));
      echo '<p>Votre recette a bien été ajoutée ! Vous pouvez désormais la consulter <a href="#">ici</a>.</p>';
   } catch (Exception $e) {
      die('Erreur : '.$e->getMessage());
   }
}




//*********************************** UPDATE *********************************** //

if (isset($_POST)) {
      try {
      $log = $_SESSION['idMembre'];
      $req = "SELECT * FROM recettes WHERE idMembre = '" . $log . "'"; //à modifier, peut-être pas avec la vue
      $whatevs = $bdd->query($req);
      $oldData = $whatevs->fetch();

      //$request = 'UPDATE membres SET (login, nom, prenom, password, mail) VALUES (:login, :nom, :prenom, :password, :mail) WHERE idMembre = "' . $log . '"';
      $request = 'UPDATE recettes SET login=:login, nom=:nom, prenom=:prenom, mail=:mail WHERE idMembre = "' . $log . '"';
      $edit = $bdd->prepare($request);
      $edit->execute(array(
         'login' => filter_var($_POST['login'], FILTER_SANITIZE_STRING),
         'nom' => filter_var($_POST['nom'], FILTER_SANITIZE_STRING),
         'prenom' => filter_var($_POST['prenom'], FILTER_SANITIZE_STRING),
         'mail' => filter_var($_POST['mail'], FILTER_SANITIZE_EMAIL)
      ));

      if (($oldData['login'] !== $_POST['login']) || ($oldData['nom'] !== $_POST['nom']) || ($oldData['prenom'] !== $_POST['prenom']) || ($oldData['mail'] !== $_POST['mail'])) {
         $dataChanged = 1;
      }
   
      $path = "location: ../profil.php?return=" . (isset($dataChanged) ? 1 : 0);
      header($path);
      exit();
   } catch (Exception $e) {
      die('Erreur : '.$e->getMessage()); 
   }
}


//*********************************** DELETE *********************************** //
$delete = $bdd->query('DELETE * FROM recettes WHERE idMembre = "' . $_SESSION['idMembre'] . '"');
$delete->exec;
header('location: ../index.php');
exit();


?>