<?php include "inc/header.php";

if (!empty($_POST)) {
   try {
      $request = 'INSERT INTO membres (login, nom, prenom, password, mail, statut) VALUES (:login, :nom, :prenom, :password, :mail, :statut)';
         $req = $bdd->prepare($request);
         $req->execute(array(
            'login' => filter_var($_POST['login'], FILTER_SANITIZE_STRING),
            'nom' => filter_var($_POST['nom'], FILTER_SANITIZE_STRING),
            'prenom' => filter_var($_POST['prenom'], FILTER_SANITIZE_STRING),
            'password' => filter_var($_POST['password'], FILTER_SANITIZE_STRING),
            'mail' => filter_var($_POST['mail'], FILTER_SANITIZE_EMAIL),
            'statut' => 'membre'
            ));
            /*$safePost = filter_input_array(INPUT_POST, [
               'login' => FILTER_SANITIZE_STRING,
               'nom' => FILTER_SANITIZE_STRING,
               'prenom' => FILTER_SANITIZE_STRING,
               'password' => FILTER_SANITIZE_STRING,
               'mail' => FILTER_SANITIZE_EMAIL

               https://stackoverflow.com/questions/44165533/php-how-to-filter-in-a-correct-way-all-post-variables

               https://github.com/markkolich/blog/blob/master/content/static/entries/the-right-way-to-parse-filter-and-validate-get-and-post-inputs-from-a-form-in-php/php-filter-input-array-example.php
            ]);*/

            //'password' => password_hash(filter_var($_POST['password'], FILTER_SANITIZE_STRING), PASSWORD_BCRYPT),

               /*$passwordHash = password_hash('$_POST['password']', PASSWORD_DEFAULT);
                  connexion : if (password_verify('bad-password', $passwordHash)) {
                                    // Correct Password
                                 } else {
                                    // Wrong password
                                 } */
            // https://phptherightway.com/#password_hashing 

            /*$_POST['login'] = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING),
            $_POST['nom'] = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING),
            $_POST['prenom'] = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_STRING),
            $_POST['password'] = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING),
            $_POST['mail'] = filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL),*/
         echo "<p>Votre inscription a été effectée.</p>";
   } catch (Exception $e) {
      die('Erreur : '.$e->getMessage()); 
   }
}
?>

<a href="index.php">> Retour à l'accueil</a>
<br><br>
<div class="container">
   <h2>Création de compte</h2><br>
   <hr>
   <form method="POST">
      Pseudo * <br>
      <input type="text" name="login" data-min="3" data-max="15" required><br>
      Nom * <br>
      <input type="text" name="nom" data-min="2" data-max="25" required><br>
      Prénom * <br>
      <input type="text" name="prenom" data-min="3" data-max="25" required><br>
      Mot de passe * <br>
      <input type="password" name="password" data-min="5" data-max="15" required><br>
      Adresse e-mail * <br>
      <input type="text" name="mail" data-min="10" data-max="20" required><br><br>
      <input type="submit" value="Valider" id="valider"><br>
   </form><br>
</div>
   
<?php include "inc/footer.php";