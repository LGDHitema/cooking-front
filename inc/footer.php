</div>

<footer class="container-fluid">
	<div class="row">
      <div class="col-sm links">
         <a href="#">Qui sommes-nous ?</a>
      </div>
      <div class="col-sm links">
         <a href="#">Nous contacter</a>
      </div>
      <div class="col-sm links">
         <a href="#">Newsletter</a>
      </div>
      <div class="col-sm links">
         <a href="#">Mentions légales</a>
      </div>
      <div class="col-sm">
         <h4>Retrouvez-nous ici :</h4>
         <a href="#">
            <img src="images/facebook.png" alt="Facebook page">
         </a>
         <a href="#">
            <img src="images/twitter.png" alt="Twitter account">
         </a>
         <a href="#">
            <img src="images/google.png" alt="Google+ account">
         </a>
         <a href="#">
            <img src="images/youtube.png" alt="Youtube account">
         </a>
      </div>
   </div>
</footer>

</body>
</html>

<!-- CREATE VIEW vue_recettes_personnes
AS SELECT m.idMembre, m.gravatar, m.login, m.prenom, r.idRecette, r.titre, r.chapo, r.img, r.preparation, r.ingredient, r.couleur, c.nom, r.dateCrea, r.tempsCuisson, r.tempsPreparation, r.difficulte, r.prix, r.parts
FROM membres m, recettes r, categories c
WHERE m.idMembre = r.membre
AND r.categorie = c.idCategorie -->