<?php session_start(); 

try {
	$bdd = new PDO('mysql:host=localhost;dbname=cooking;charset=latin1', 'root', '');
	} catch(Exception $e) { 
	die('Erreur : '.$e->getMessage()); 
}
?>


<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Cooking</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link rel="stylesheet" href="style.css">
		<meta name="keywords" content="cooking, cuisine, recettes, repas, gastronomie, minceur, diet, miam miam">
		<meta name="description" content="Le site Cooking regroupe pléthore de recettes des plus variées, customisé par des recettes d'internautes">
		<meta name="author" content="Gollain Dupont Ludivine">
	</head>


	<header class="container-fluid">
		<div class="row">
			<div class="col-sm-4 rounded text-center" id="hr1">
				<a href="index.php"><img src="images/logo-cooking.png" alt="logo" id="logo"></a>
				<h5 id ="slogan">Miam miam, gloup gloup, laps laps</h5>
			</div>
			<div class="col-sm-4" id="search">
				<form class="form-inline my-2 my-lg-0" method="GET" action="search.php">
					<input class="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search" id="search2" name="search" data-max="20">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Recherche</button>
				</form>
			</div>
			<div class="col-sm-4" id="identifiants">
				<?php
					if(!isset($_SESSION['idMembre'])) { ?>
						<button class="btn btn-primary" type="submit" onclick="javascript:location.href='connexion.php'">Connexion</button>
						<button type="button" class="btn btn-outline-primary" onclick="javascript:location.href='inscription.php'">Se créer un compte</button>
					<?php } else { ?>
						<button class="btn btn-primary" type="submit" onclick="javascript:location.href='profil.php'">Mon compte</button>
						<a href="deconnexion.php">Déconnexion</a>
				<?php } ?>
			</div>
		</div>
	</header>

   <div id="menu">
		<ul class="nav justify-content-center">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Recettes</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="#">Par catégorie :</a>
					<a class="dropdown-item" href="search.php?option=fruit">-> Fruits</a>
					<a class="dropdown-item" href="search.php?option=viande">-> Viandes</a>
					<a class="dropdown-item" href="search.php?option=legume">-> Légumes</a>
					<a class="dropdown-item" href="search.php?option=poisson">-> Poissons</a>
						<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="search.php?option=moins-cheres">Les - chères</a>
					<a class="dropdown-item" href="search.php?option=plus-rapides">Les + rapides</a>
					<a class="dropdown-item" href="search.php?option=plus-faciles">Les + faciles</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="search.php?option=plats">Plats</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="search.php?option=desserts">Desserts</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="search.php?option=minceur">Minceur</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="atelier.php">Atelier</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="ajouter-recette.php">Déposer une recette</a>
			</li>
		</ul>
	</div>

<body>
   <div class="container" id="mainBod">