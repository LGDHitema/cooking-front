<?php include "inc/header.php"; 

	if(isset($_GET['search'])) {
		$mot = $_GET['search'];
		$recherche = filter_var($mot, FILTER_SANITIZE_STRING);
		$datas = $bdd->query("SELECT * FROM recettes WHERE titre LIKE '%" . $recherche . "%' OR chapo LIKE '%" . $recherche . "%'");
	} elseif (isset($_GET['option'])) {
		$op = $_GET['option'];
		$option = filter_var($op, FILTER_SANITIZE_STRING);
		switch($option) {
			case 'fruit':
				$datas = $bdd->query("SELECT * FROM categories c, recettes r WHERE r.categorie = c.idCategorie AND c.nom = 'fruit'");
			break;
			case 'viande':
				$datas = $bdd->query("SELECT * FROM categories c, recettes r WHERE r.categorie = c.idCategorie AND c.nom = 'viande'");
			break;
			case 'legume':
			$datas = $bdd->query("SELECT * FROM categories c, recettes r WHERE r.categorie = c.idCategorie AND c.nom = 'legume'");
			break;
			case 'poisson':
			$datas = $bdd->query("SELECT * FROM categories c, recettes r WHERE r.categorie = c.idCategorie AND c.nom = 'poisson'");
			break;
			case 'moins-cheres':
			$datas = $bdd->query("SELECT * FROM recettes WHERE prix = 'Pas cher'");
			break;
			case 'plus-rapides':
			$datas = $bdd->query("SELECT * FROM recettes WHERE tempsCuisson <= '1h'"); // Modifier la BDD pour que le temps soit en minute et pas en string
			break;
			case 'plus-faciles':
			$datas = $bdd->query("SELECT * FROM recettes WHERE difficulte = 'Facile'");
			break;
			default:
				// pas de requêtes par défauts
			break;
		}
	} ?>

	<a href='index.php'>> Retour à l'accueil</a><br><br>
	<div class='container'>

	<?php if(isset($datas)) {
				if($datas) { ?>
					<h2>Recettes disponibles : </h2><br><br><br>
						<?php while ($data = $datas->fetch()) { ?>
							<ul><li><a href="recette-detail.php?id=<?= $data['idRecette'] ?>"><?= $data['titre'] ?></a><br><br></li></ul>
						<?php }
					} else { ?>
						<p>Pas de recettes correspondants à la recherche...</p>
					<?php }
				} ?>
	</div>

<?php include "inc/footer.php";