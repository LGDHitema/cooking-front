<?php include "inc/header.php" ?>

<a href="index.php">> Retour à l'accueil</a>
<br><br>


   <?php $list = $bdd->query('SELECT * FROM vue_recettes_personnes ORDER BY login ASC');
			while($members = $list->fetch()) { ?>

      <div id="carte" style="border:solid;">
         <div id="top">
         <img src='photos/gravatars/<?= $members['gravatar'] ?>' alt='membre' class='img-thumbnail'>
            <p><?= $members['login'] ?></p>
         </div>
         <hr>

         <div id="corps">
            <p>
               <img src="photos/recettes/<?= $members['img'] ?>" class="img-fluid rounded" alt="Responsive image" style="max-height:175px;">
               <br>
               <?= $members['titre']?>
            </p>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Et plus...</button>
            <!-- ********* Modal ******** -->
         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <h4 class="modal-title" id="exampleModalLabel"><?= $members['login'] ?></h4><br>
                     <img src="photos/gravatars/<?= $members['gravatar'] ?>" id="avatar">
                  </div>
                  <div class="modal-body">
                  <h5><?= $members['prenom'] ?></h5>
                     <p>
                        <a href="#"><img src="images/facebook.png" alt="Facebook page"></a>
                        <a href="#"><img src="images/twitter.png" alt="twitter page"></a>
                        <a href="#"><img src="images/google.png" alt="google page"></a>
                        <a href="#"><img src="images/youtube.png" alt="youtube page"></a>
                        <hr>
                     </p> <!-- Faire un IF si la personne a déjà filé des recettes ou non, si non : "Oups, cette personne n'a pas encore partagé de recettes avec nous ;p !"
                     while pour afficher toutes les recettes avec fetch ou fetchAll? de disponibles, avec : la photo, le titre, le début de chapo, et le résumé de préparation (parts, coût, prix, difficulté,.......) image haut à gauche, haut droit : titre + details, et chapo en dessous des deux, pus un hr pour l'autre recette -->
                     </p>
                  </div>
               </div>
            </div>
         </div>
            </div>
      </div>
   <?php } ?>

<div class="row" id="recettes">
		<?php
			$cards = $bdd->query('SELECT * FROM recettes ORDER BY dateCrea ASC');
			while($donneesCards = $cards->fetch()) {
				$membre = $bdd->query('SELECT * FROM membres WHERE idMembre = "' . $donneesCards['membre'] . '"');
				$membres = $membre->fetch(); ?>
				<div class='col-12 col-md-6 col-lg-4 mt-3'>
					<div class='card'>
                  <img src="photos/gravatars/<?= $membres['gravatar'] ?>" id="avatar" style="float:left;">
					<div class='card-body'>
						<h5 class='card-title'><?= $donneesCards['titre'] ?></h5>
						<p class='card-text justify'><?= $donneesCards['chapo'] ?></p>
					</div>
					<div class='card-footer'>
						<h5>Proposée par :</h5>
						<img src='photos/gravatars/<?= $membres['gravatar'] ?>' alt='membre' class='img-thumbnail' onclick="javascript:location.href='membre-detail.php'">
						<p><?= $membres['prenom'] ?></p>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

   </div>

<?php include "inc/footer.php";