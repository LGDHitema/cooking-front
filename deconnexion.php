<?php include "inc/header.php";
   session_destroy();
   header('location: index.php');
   include "inc/footer.php";
?>

<!-- 
********* Ancienne version qui fonctionnait bien mais trop complexe *********

<p>Vous avez été déconnecté.e.</p><br>
<script>
   function Redirect() {
      window.location = "index.php";
   }
   document.write("Vous allez être redirigé.e dans 5 secondes.");
   setTimeout("Redirect()", 5000)
</script>
<p><a href="index.php">Retour à l'accueil directement.</a></p> -->

