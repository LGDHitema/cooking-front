<?php include "inc/header.php" ?>

<a href="index.php">> Retour à l'accueil</a>
<br><br><br>
<?php
   $id = $_GET['id'];
   $query = $bdd->query("SELECT * FROM vue_recettes_personnes WHERE idRecette = '" . $id . "'");
   $recette = $query->fetch(); ?>
<h2><?= $recette['titre'] ?></h2><br>

<!-- SI on est co et que la recette que l'on voit est la nôtre, ajouter une icône pour la modifier directement -->

<br>
<img src="photos/recettes/<?= $recette['img'] ?>" class="img-fluid rounded mx-auto d-block" alt="Responsive image" id="photoRecette">
<br><br>
<div class="row">
   <div class="col-sm-4" id="ingredients">
      <h4>Ingredients</h4>
      <hr>
      <ul id="recettesInfos">
         <li><img src="images/temps.png" alt="temps">  Temps de préparation : <?= $recette['tempsPreparation'] ?></li>
         <li><img src="images/cuisson.png" alt="cuisson">  Temps de cuisson : <?= $recette['tempsCuisson'] ?></li>
         <li><img src="images/fourchette.png" alt="fourchette">  Pour 
            <?php if (empty($recette['parts'])) { 
               echo "?"; } else { 
               echo $recette['parts']; } ?> personnes</li>
         <li><img src="images/prix.png" alt="prix">  Coût : <?= $recette['prix'] ?></li>
         <li>Difficulté : <?= $recette['difficulte'] ?></li>
      </ul>
      <hr>
      <?= $recette['ingredient'] ?>
   </div>
   <div class="col-sm-7" id="details">
      <h4>Préparation</h4>
      <hr>
      <div id="preparation" class="justify">
         <?= $recette['preparation'] ?>
      </div>
   </div>
</div>

<?php include "inc/footer.php";